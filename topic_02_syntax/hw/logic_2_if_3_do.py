"""
Функция if_3_do.

Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""

def if_3_do(i):
    if i > 3:
        i += 10
        return i
    else:
        i -= 10
        return i


if __name__ == '__main__':
    result = if_3_do(7)
    print(result)
