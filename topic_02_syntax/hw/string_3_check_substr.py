"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""

def check_substr(str1, str2):
    if len(str1) < len(str2):
        return str1 in str2
    elif len(str2) < len(str1):
        return str2 in str1
    else:
        return False

if __name__=='__main__':
    print(check_substr('8564649846412368479864', '123'))
