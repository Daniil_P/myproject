"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""

def count_odd_num(n):
    odd = 0

    if type(n) != int:
        return "Must be int!"

    elif 0 >= n:
        return "Must be > 0!"

    else:
        while n > 0:
            if n % 2 != 0:
                odd += 1
            n = n // 10
        return odd

if __name__ == '__main__':
    result = count_odd_num(54485470)
    print(result)
