"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""

def print_symbols_if(string):
    length = len(string)
    if not length:
        print('Empty string!')
    elif length > 5:
        print(string[0:3] + string[length-3:length])
    else:
        print(string[0] * length)


if __name__ == '__main__':
    print_symbols_if('')
    print_symbols_if('25')
    print_symbols_if('9876543210')

